import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/providers/todo_provider.dart';
import 'package:todo_app/screens/add_item_todo_list.dart';
import 'package:todo_app/widgets/todo_list.dart';

class TodoListScreen extends StatelessWidget {
  static const routeName = '/todo_list_screen';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Todo List',
          style: Theme.of(context).textTheme.headline4,
        ),
      ),
      body: Consumer<TodoProvider>(
        builder: (_, todoTaskProvider, __) =>
            TodoList(todoTaskProvider.categories),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => {
          Navigator.of(context)
              .pushNamed(AddItemTodoList.routeName, arguments: 0),
        },
      ),
    );
  }
}
