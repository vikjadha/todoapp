import 'package:flutter/material.dart';
import 'package:todo_app/screens/todo_list_screen.dart';
import 'package:todo_app/widgets/common_button.dart';

class HomeScreen extends StatefulWidget {
  static const routeName = '/homescreen';
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

Widget _firstPage(BuildContext context) {
  return Center(
    child: Container(
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          style: Theme.of(context).textTheme.headline6,
          children: <TextSpan>[
            TextSpan(
              text: 'Welcome to ',
              style: new TextStyle(fontSize: 35.0),
            ),
            TextSpan(
              text: 'Clear\n\n',
              style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 35.0),
            ),
            TextSpan(
              text: 'Tap or Swipe to begin.',
            )
          ],
        ),
      ),
    ),
  );
}

Widget _secondPage(BuildContext context) {
  return Padding(
    padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 60.0),
    child: Column(
      children: <Widget>[
        Spacer(),
        RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            style: Theme.of(context).textTheme.headline6,
            children: <TextSpan>[
              TextSpan(
                text: 'Clear sorts items by ',
              ),
              TextSpan(
                text: 'Priority\n\n',
                style: new TextStyle(fontWeight: FontWeight.bold),
              ),
              TextSpan(
                text: 'Important items are highlighted at the top....',
              )
            ],
          ),
        ),
        Spacer(),
        Image.asset(
          'assets/images/page2.png',
          width: 320,
        ),
      ],
    ),
  );
}

Widget _thirdPage(BuildContext context) {
  return Padding(
    padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 60.0),
    child: Column(
      children: <Widget>[
        Spacer(),
        RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            style: Theme.of(context).textTheme.headline6,
            children: <TextSpan>[
              TextSpan(
                text: 'Tap and hold ',
                style: new TextStyle(fontWeight: FontWeight.bold),
              ),
              TextSpan(
                text:
                    'to pick an item up.\n\nDrag it up or down to change its priority.',
              )
            ],
          ),
        ),
        Spacer(),
        Image.asset(
          'assets/images/page3.png',
          width: 320,
        ),
      ],
    ),
  );
}

Widget _fourthPage(BuildContext context) {
  return Padding(
    padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 60.0),
    child: Column(
      children: <Widget>[
        Spacer(),
        Text(
          'There are three navigation levels...',
          style: Theme.of(context).textTheme.headline6,
        ),
        Spacer(),
        Image.asset(
          'assets/images/page4.png',
          width: 320,
        ),
      ],
    ),
  );
}

Widget _fivePage(BuildContext context) {
  return Padding(
    padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 60.0),
    child: Column(
      children: <Widget>[
        Spacer(),
        RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            style: Theme.of(context).textTheme.headline6,
            children: <TextSpan>[
              TextSpan(
                text: 'Pinch together vertically ',
                style: new TextStyle(fontWeight: FontWeight.bold),
              ),
              TextSpan(
                text: 'to collapse your current level and navigate up.',
              )
            ],
          ),
        ),
        Spacer(),
        Image.asset(
          'assets/images/page5.png',
          width: 320,
        ),
      ],
    ),
  );
}

Widget _sixthPage(BuildContext context) {
  return Padding(
    padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 60.0),
    child: Column(
      children: <Widget>[
        Spacer(),
        RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            style: Theme.of(context).textTheme.headline6,
            children: <TextSpan>[
              TextSpan(
                text: 'Tap on a list ',
                style: new TextStyle(fontWeight: FontWeight.bold),
              ),
              TextSpan(
                text: 'to see its content.\n',
              ),
              TextSpan(
                text: 'Tap on a list title ',
                style: new TextStyle(fontWeight: FontWeight.bold),
              ),
              TextSpan(
                text: 'to edit it....',
              ),
            ],
          ),
        ),
        Spacer(),
        Image.asset(
          'assets/images/page6.png',
          width: 320,
        ),
      ],
    ),
  );
}

Widget _sevenPage(BuildContext context) {
  return Padding(
    padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 60.0),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Image.asset(
          'assets/images/cloud.png',
          width: 280,
        ),
        Container(
          margin: EdgeInsets.only(top: 30.0, bottom: 50.0),
          child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: Theme.of(context).textTheme.headline6,
              children: <TextSpan>[
                TextSpan(
                  text: 'Use ',
                  style: new TextStyle(fontSize: 35.0),
                ),
                TextSpan(
                  text: 'iCloud?\n\n',
                  style: new TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 35.0),
                ),
                TextSpan(
                  text:
                      'Storing your lists in iCloud allows you to keep your data in sync across your iPhone, iPad and Mac.',
                ),
              ],
            ),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            CommonButton(
              title: 'Not Now',
              onPressed: (_) {},
            ),
            CommonButton(title: 'Use iCloud', onPressed: (_) {}),
          ],
        )
      ],
    ),
  );
}

Widget _eightPage(BuildContext context) {
  return Center(
    child: SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 60.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
                'Sign up to the newsletter, and unlock a theme for your lists.',
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline6),
            Container(
              margin: EdgeInsets.symmetric(vertical: 30.0),
              child: Image.asset(
                'assets/images/mail.png',
                width: 280,
              ),
            ),
            Container(
              margin: EdgeInsetsDirectional.only(bottom: 16.0),
              child: TextField(
                style: TextStyle(color: Colors.black),
                decoration: InputDecoration(
                  hintText: 'Email Address',
                  hintStyle: TextStyle(color: Colors.grey),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                CommonButton(
                  title: 'Skip',
                  onPressed: (context) {
                    _getInsideApp(context);
                  },
                ),
                CommonButton(
                  title: 'Join',
                  onPressed: (context) {
                    _getInsideApp(context);
                  },
                ),
              ],
            )
          ],
        ),
      ),
    ),
  );
}

void _getInsideApp(BuildContext context) {
  Navigator.of(context).pushReplacementNamed(TodoListScreen.routeName);
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 233, 227, 230),
      body: PageView(
        children: <Widget>[
          _firstPage(context),
          _secondPage(context),
          _thirdPage(context),
          _fourthPage(context),
          _fivePage(context),
          _sixthPage(context),
          _sevenPage(context),
          _eightPage(context),
        ],
      ),
    );
  }
}
