import 'package:flutter/material.dart';
import 'package:todo_app/widgets/add_item_todo_widget.dart';

class AddItemTodoList extends StatelessWidget {
  static const routeName = '/additem';

  @override
  Widget build(BuildContext context) {
    final int argsCategoryId = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.black26,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
      ),
      body: SingleChildScrollView(
        child: AddItemTodoWidget(argsCategoryId),
      ),
    );
  }
}
