import 'package:flutter/material.dart';

class CommonButton extends StatelessWidget {
  final String title;
  final Function onPressed;

  CommonButton({this.title, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      onPressed: () {
        onPressed(context);
      },
      style: OutlinedButton.styleFrom(
        padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 25.0),
        primary: Colors.black,
        textStyle: Theme.of(context).textTheme.headline6,
        side: BorderSide(color: Colors.black, width: 1.5),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
        ),
      ),
      child: Text(title),
    );
  }
}
