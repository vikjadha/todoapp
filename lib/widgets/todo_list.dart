import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/models/create_task.dart';
import 'package:todo_app/providers/todo_provider.dart';
import 'package:todo_app/utils/global.dart';
import 'package:todo_app/widgets/list_card_widget.dart';

class TodoList extends StatelessWidget {
  final List<dynamic> items;

  TodoList(this.items);

  void _deleteItem(BuildContext context, int index) {
    var item = items[index];
    if (item is CreateTask) {
      Provider.of<TodoProvider>(context, listen: false)
          .deleteTask(item.categoryId, index);
    } else {
      Provider.of<TodoProvider>(context, listen: false).deleteCategory(index);
    }
  }

  void _completeItem(BuildContext context, int index) {
    var item = items[index];

    _deleteItem(context, index);

    if (item is CreateTask) {
      Provider.of<TodoProvider>(context, listen: false)
          .completeTask(_updatedItem(item, !item.isCompleted));
    } else {
      Provider.of<TodoProvider>(context, listen: false)
          .completeCategory(_updatedItem(item, !item.isCompleted));
    }
  }

  dynamic _updatedItem(dynamic item, bool _isCompleted) {
    if (item is CreateTask) {
      return CreateTask(
        categoryId: item.categoryId,
        name: item.name,
        reminder: item.reminder,
        isCompleted: _isCompleted,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    int itemCount = items.length;
    return Container(
      child: ReorderableListView.builder(
        keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
        shrinkWrap: true,
        itemCount: itemCount,
        itemBuilder: (context, index) {
          return _createRow(context, index);
        },
        onReorder: (oldIndex, newIndex) =>
            _onReorderRow(context, oldIndex, newIndex),
      ),
    );
  }

  _createRow(BuildContext context, int index) {
    return ListCardWidget(
      key: ValueKey(items[index]),
      item: items[index],
      index: index,
      color: GlobalColor.todoItemColorForIndex(index, items.length, true),
      deleteItem: (index) => _deleteItem(context, index),
      completeItem: (index) => _completeItem(context, index),
    );
  }

  void _onReorderRow(BuildContext context, oldIndex, newIndex) {
    int _completedTaskStartIndex = items.indexWhere((item) => item.isCompleted);

    if (oldIndex < newIndex) {
      newIndex -= 1;
    }

    var item = items[oldIndex];
    _deleteItem(context, oldIndex);

    bool isNewIndexAfterCompletedTaskIndex =
        _completedTaskStartIndex > -1 && newIndex >= _completedTaskStartIndex;

    if (item is CreateTask) {
      Provider.of<TodoProvider>(context, listen: false).addTaskAt(
          newIndex, _updatedItem(item, isNewIndexAfterCompletedTaskIndex));
    } else {
      Provider.of<TodoProvider>(context, listen: false).addCategoryAt(
          newIndex, _updatedItem(item, isNewIndexAfterCompletedTaskIndex));
    }
  }
}
