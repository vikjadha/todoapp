import 'package:flutter/material.dart';
import 'package:todo_app/utils/global.dart';

class ListCardWidget extends StatefulWidget {
  final ValueKey key;
  final int index;
  final dynamic item;
  final Color color;
  final Function deleteItem;
  final Function completeItem;

  ListCardWidget({
    @required this.item,
    @required this.index,
    @required this.key,
    @required this.color,
    @required this.deleteItem,
    @required this.completeItem,
  });

  @override
  _ListCardWidgetState createState() => _ListCardWidgetState();
}

class _ListCardWidgetState extends State<ListCardWidget> {
  Widget _buildTitle(BuildContext context) {
    return Text(
      widget.item.name,
      overflow: TextOverflow.ellipsis,
      style: widget.item.isCompleted
          ? Theme.of(context).textTheme.headline5.merge(
                TextStyle(
                  color: Colors.grey,
                  decoration: TextDecoration.lineThrough,
                ),
              )
          : Theme.of(context).textTheme.headline5,
    );
  }

  void _onDissmissed(DismissDirection direction) {
    if (direction == DismissDirection.endToStart) {
      widget.deleteItem(widget.index);
    } else if (direction == DismissDirection.startToEnd) {
      widget.completeItem(widget.index);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Dismissible(
        key: widget.key,
        onDismissed: _onDissmissed,
        child: Container(
          height: 62.0,
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
            color: widget.color,
          ),
          foregroundDecoration: GlobalColor.todoItemGradient(),
          child: ListTile(
            title: _buildTitle(context),
          ),
        ),
        background: Container(
          color: Colors.black,
          padding: EdgeInsets.only(left: 20.0),
          alignment: Alignment.centerLeft,
          child: Icon(Icons.check, size: 36.0),
        ),
        secondaryBackground: Container(
          color: Colors.black,
          padding: EdgeInsets.only(right: 20.0),
          alignment: Alignment.centerRight,
          child: Icon(Icons.close, size: 36.0, color: Colors.red),
        ),
      ),
    );
  }
}
