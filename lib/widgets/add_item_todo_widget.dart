import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/providers/todo_provider.dart';
import 'package:todo_app/utils/global.dart';

class AddItemTodoWidget extends StatelessWidget {
  final int categoryId;

  AddItemTodoWidget(this.categoryId);

  bool isTask() {
    return categoryId != 0;
  }

  Widget build(BuildContext context) {
    final _fieldBgColor = GlobalColor.textFieldColor(isTask());
    final _cursorColor = GlobalColor.textFieldColor(!isTask());
    return Material(
      child: Container(
        child: Column(
          //mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              color: _fieldBgColor,
              child: TextField(
                onSubmitted: (value) {
                  FocusScope.of(context).unfocus();
                  if (value.length > 0) {
                    if (isTask()) {
                      Provider.of<TodoProvider>(context, listen: false)
                          .addTask(categoryId, value);
                    } else {
                      Provider.of<TodoProvider>(context, listen: false)
                          .addCategory(value);
                    }
                    Navigator.of(context).pop();
                  } else {
                    Navigator.of(context).pop();
                  }
                },
                textInputAction: TextInputAction.done,
                style: Theme.of(context).textTheme.headline5,
                cursorColor: _cursorColor,
                decoration: InputDecoration(
                  border: GlobalColor.outlineInputBorder(_fieldBgColor),
                  focusedBorder: GlobalColor.outlineInputBorder(_fieldBgColor),
                  enabledBorder: GlobalColor.outlineInputBorder(_fieldBgColor),
                  hintStyle: TextStyle(color: Colors.grey),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
