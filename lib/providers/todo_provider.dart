import 'package:flutter/material.dart';
import 'package:todo_app/models/category_model.dart';
import 'package:todo_app/models/create_task.dart';

class TodoProvider extends ChangeNotifier {
  List<CategoryModel> _categories = [];

  TodoProvider() {
    _categories.add(_getDummyCategory());
  }

  void addCategory(String name) {
    _categories.insert(0, CategoryModel(name: name, tasks: []));
    notifyListeners();
  }

  void addCategoryAt(int index, CategoryModel category) {
    _categories.insert(index, category);
    notifyListeners();
  }

  void deleteCategory(int index) {
    _categories.removeAt(index);
    notifyListeners();
  }

  void completeCategory(CategoryModel category) {
    _categories.add(category);
    notifyListeners();
  }

  void addTask(int categoryId, String name) {
    CategoryModel category = _getCategoryFromId(categoryId);
    category.addTask(name);
    notifyListeners();
  }

  void addTaskAt(int index, CreateTask task) {
    CategoryModel category = _getCategoryFromId(task.categoryId);
    category.addTaskAt(index, task);
    notifyListeners();
  }

  void deleteTask(int categoryId, int index) {
    CategoryModel category = _getCategoryFromId(categoryId);
    category.deleteTask(index);
    notifyListeners();
  }

  void completeTask(CreateTask task) {
    CategoryModel category = _getCategoryFromId(task.categoryId);
    category.completeTask(task);
    notifyListeners();
  }

  List<CategoryModel> get categories {
    return _categories;
  }

  CategoryModel _getCategoryFromId(int categoryId) {
    return _categories.firstWhere((item) => item.id == categoryId);
  }

  CategoryModel _getDummyCategory() {
    CategoryModel dummyCategory = CategoryModel(name: 'Dummy List', tasks: []);
    dummyCategory.addTask('Pick up dry cleaning');
    dummyCategory.addTask('Get a haircut');
    dummyCategory.addTask('Move the lawn');
    dummyCategory.addTask('Buy groceries');
    dummyCategory.addTask('Go to the gym');
    return dummyCategory;
  }
}
