import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/providers/todo_provider.dart';
import 'package:todo_app/screens/add_item_todo_list.dart';
import 'package:todo_app/screens/home.dart';
import 'package:todo_app/screens/todo_list_screen.dart';
import 'package:todo_app/utils/global.dart';
import 'package:todo_app/widgets/routes.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => TodoProvider(),
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          brightness: Brightness.dark,
          primarySwatch: Colors.grey,
          scaffoldBackgroundColor: Colors.black,
          textTheme: ThemeData.light().textTheme.copyWith(
                headline4: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
                headline5: TextStyle(fontSize: 22.0, color: Colors.white),
                headline6: TextStyle(
                    fontSize: 22.0,
                    fontWeight: FontWeight.normal,
                    color: Colors.black),
                subtitle1: TextStyle(fontSize: 15.0, color: Colors.white),
              ),
          appBarTheme: ThemeData.light().appBarTheme.copyWith(
                centerTitle: true,
              ),
          inputDecorationTheme: InputDecorationTheme(
            border: GlobalColor.outlineInputBorder(Colors.black),
            focusedBorder: GlobalColor.outlineInputBorder(Colors.black),
            enabledBorder: GlobalColor.outlineInputBorder(Colors.black),
          ),
        ),
        home: HomeScreen(),
        onGenerateRoute: (RouteSettings settings) {
          switch (settings.name) {
            case TodoListScreen.routeName:
              return Routes(
                  builder: (_) => TodoListScreen(), settings: settings);
            case AddItemTodoList.routeName:
              return Routes(
                  builder: (_) => AddItemTodoList(), settings: settings);
            default:
              return Routes(
                  builder: (_) => TodoListScreen(), settings: settings);
          }
        },
      ),
    );
  }
}
