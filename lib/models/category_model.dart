import 'package:todo_app/models/create_task.dart';

class CategoryModel {
  final int id = DateTime.now().microsecondsSinceEpoch;
  final String name;
  List<CreateTask> tasks = [];
  final bool isCompleted;

  CategoryModel({
    this.name,
    this.isCompleted = false,
    this.tasks,
  });

  void addTask(String name) {
    tasks.insert(0, CreateTask(categoryId: id, name: name));
  }

  void addTaskAt(int index, CreateTask task) {
    tasks.insert(index, task);
  }

  void completeTask(CreateTask task) {
    tasks.add(task);
  }

  void deleteTask(int index) {
    tasks.removeAt(index);
  }

  int get taskCount {
    return tasks.length;
  }
}
