import 'package:flutter/foundation.dart';

class CreateTask {
  final int id = DateTime.now().microsecondsSinceEpoch;
  final int categoryId;
  final String name;
  final DateTime reminder;
  final bool isCompleted;

  CreateTask({
    @required this.categoryId,
    @required this.name,
    this.reminder,
    this.isCompleted = false,
  });
}
