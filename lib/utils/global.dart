import 'package:flutter/material.dart';

class GlobalColor {
  static Color todoItemColorForIndex(int index, int count, bool isTask) {
    int val = ((index / (count)) * 225).toInt();
    return isTask
        ? Color.fromARGB(255, 255, val, 0)
        : Color.fromARGB(255, 0, val, 255);
  }

  static Color textFieldColor(bool isTask) {
    return todoItemColorForIndex(0, 1, isTask);
  }

  static BoxDecoration todoItemGradient() {
    return BoxDecoration(
      gradient: LinearGradient(
        begin: Alignment(-1, -1),
        end: Alignment(-1, 0.01),
        colors: [Colors.black12, Colors.transparent],
      ),
    );
  }

  static OutlineInputBorder outlineInputBorder(Color borderColor) {
    return OutlineInputBorder(
      borderSide: BorderSide(color: borderColor),
      borderRadius: BorderRadius.all(Radius.circular(8.0)),
    );
  }
}
